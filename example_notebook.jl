### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ ddd247ac-e74a-11eb-378f-f9b507d0638f
begin
	import Pkg; Pkg.add("Plots");
	import Pkg; Pkg.add("PlutoUI");
	using Plots
	using PlutoUI
	using Random
	plotly();
	Random.seed!(666);
end

# ╔═╡ 2e2c218f-82be-47ab-bc27-f6392bb66916
md"# TrigPoly Notebook"

# ╔═╡ 1d941dd1-e7a1-4017-858a-93487749a6ec
md"""
n: $(@bind n Slider(1:1024; default=64, show_value=true))
Final Terms: $(@bind _t Slider(1:10240; default=32, show_value=true))
Terms: $(@bind tm Slider(1:10240; default=64, show_value=true))
Total Points: $(@bind tot Slider(1:10240; default=128, show_value=true))
"""

# ╔═╡ a3c4aa4e-f6f1-4a50-81f3-a10caac9daaa
yy = rand(n).-.5;

# ╔═╡ 2364386c-06eb-4caf-b6c1-9339ba8e5d91
yb = yy;

# ╔═╡ cfa7df8c-89d6-493a-a25e-357b00367eac
begin
	# catch any out of bounds things
	if tm > n
		t = n;
	else
		if tm < 1
			t = 1;
		end
		t = _t;
	end
	if _t > n
		t = n;
	end
end

# ╔═╡ 68864f96-2025-41b0-99da-cd37f2a6b1e3
for i in 2:n
    yb[i] = yb[i-1] + yy[i]
end

# ╔═╡ 789eeb2c-45b0-4add-a0ed-e293d97e8f8c
y = yb;

# ╔═╡ 53fd85c1-b869-4d99-b81c-97e21ca16f9d
m = (last(y) - first(y)) / (n-1);

# ╔═╡ ea56e197-2722-4940-8e4b-d9d751369f65
b = first(y);

# ╔═╡ 2b4ec09a-275f-4a38-8075-9d10e0a2fdac
slope_line = [(m*x)+b for x in collect(0:n-1)];

# ╔═╡ ee767f20-9c42-4a29-8e11-b86a5d59aabe
begin
	plot(y, label = "Random Time Series", lw = 3, color=:blue, title="Data")
	plot!(slope_line, lw=3, linestyle = :dash, color=:green, label= "Slope Line")
end

# ╔═╡ f9790d3e-278d-473b-93cc-541a7d3196cf
desloped_y = y - slope_line;

# ╔═╡ 4c958fd2-d620-4d1f-9fcb-b796221dd133
plot(desloped_y, label = "De-Sloped Data", lw = 3, color=:blue, title="De-Sloped Data")

# ╔═╡ edabe4bd-4b78-4a93-a868-5dae2908c671
afac = 2/(n-1);

# ╔═╡ 7a5df419-5756-42c0-88c8-7ae9ec7b5a26
s = n/tot;

# ╔═╡ be08ee55-78f6-4ce3-be53-c0e27c4d4c07
TPa = zeros(n,t);

# ╔═╡ 914ba4a9-e111-454c-978c-f24e4f1e5703
for i=0:n-1, j=0:t-1
	TPa[i+1,j+1]=sin((j*π)*(i/(n-1)))
end

# ╔═╡ ec0c9187-ae9d-40cf-80ac-e7018a4baedf
a = zeros(n);

# ╔═╡ 1ea79712-bb4f-4b31-856c-92998240a376
plot(a)

# ╔═╡ cf72219a-0a18-4798-993f-e590032e3601
for j=1:t
	tmp=0
	for i=1:n
		tmp += desloped_y[i] * TPa[i,j]  
	end
	a[j] = tmp * afac
end

# ╔═╡ 6784d905-0e00-4ba2-b047-f8f8e1a1e67a
TPmodel = zeros(tot);

# ╔═╡ a71b2a80-f962-45a1-8997-a14522efe82d
for x in 0:tot-1
	tp = 0
	for j in 0:t-1
            tp = tp + (a[j+1] * sin( j * π * ((x * s)/(n-1))));
	end
	TPmodel[x+1] = (m * (x * s) + b) + tp
end

# ╔═╡ 9d9844f4-c285-45e9-94ce-f5fe216bab6a
X0= collect(0:n-1);

# ╔═╡ 281f3d4a-321b-42d9-8373-d0bb40901299
X1 = collect(0:tot-1) * s;

# ╔═╡ bc5879d9-783e-4d8a-8971-914a2b998f39
Y0= y;

# ╔═╡ f564e206-e544-4da0-8fc4-1604224a6e81
Y1 = TPmodel;

# ╔═╡ 2cc7918b-7804-4b56-8eed-534cb6e797c3
begin
	plot(X0,Y0, label="Data", lw = 1, markersize=2, color=:blue,seriestype=:scatter, title="TrigPoly Model: n t=$t tm=$tm tot=$tot")
	plot!(X1,Y1, label="Model", lw=2, seriestype=:line, linestyle=:dot, color=:red)
end

# ╔═╡ Cell order:
# ╟─2e2c218f-82be-47ab-bc27-f6392bb66916
# ╠═1d941dd1-e7a1-4017-858a-93487749a6ec
# ╟─ee767f20-9c42-4a29-8e11-b86a5d59aabe
# ╠═1ea79712-bb4f-4b31-856c-92998240a376
# ╟─4c958fd2-d620-4d1f-9fcb-b796221dd133
# ╟─2cc7918b-7804-4b56-8eed-534cb6e797c3
# ╟─ddd247ac-e74a-11eb-378f-f9b507d0638f
# ╟─a3c4aa4e-f6f1-4a50-81f3-a10caac9daaa
# ╟─2364386c-06eb-4caf-b6c1-9339ba8e5d91
# ╟─cfa7df8c-89d6-493a-a25e-357b00367eac
# ╟─68864f96-2025-41b0-99da-cd37f2a6b1e3
# ╟─789eeb2c-45b0-4add-a0ed-e293d97e8f8c
# ╟─53fd85c1-b869-4d99-b81c-97e21ca16f9d
# ╟─ea56e197-2722-4940-8e4b-d9d751369f65
# ╟─2b4ec09a-275f-4a38-8075-9d10e0a2fdac
# ╟─f9790d3e-278d-473b-93cc-541a7d3196cf
# ╟─edabe4bd-4b78-4a93-a868-5dae2908c671
# ╟─7a5df419-5756-42c0-88c8-7ae9ec7b5a26
# ╟─be08ee55-78f6-4ce3-be53-c0e27c4d4c07
# ╟─914ba4a9-e111-454c-978c-f24e4f1e5703
# ╟─ec0c9187-ae9d-40cf-80ac-e7018a4baedf
# ╟─cf72219a-0a18-4798-993f-e590032e3601
# ╟─6784d905-0e00-4ba2-b047-f8f8e1a1e67a
# ╟─a71b2a80-f962-45a1-8997-a14522efe82d
# ╟─9d9844f4-c285-45e9-94ce-f5fe216bab6a
# ╟─281f3d4a-321b-42d9-8373-d0bb40901299
# ╟─bc5879d9-783e-4d8a-8971-914a2b998f39
# ╟─f564e206-e544-4da0-8fc4-1604224a6e81
