using TrigPolly
using Documenter

DocMeta.setdocmeta!(TrigPolly, :DocTestSetup, :(using TrigPolly); recursive=true)

makedocs(;
    modules=[TrigPolly],
    authors="Matthew Camp",
    repo="https://github.com/mcamp/TrigPolly.jl/blob/{commit}{path}#{line}",
    sitename="TrigPolly.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mcamp.gitlab.io/TrigPolly.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
