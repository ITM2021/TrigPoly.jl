```@meta
CurrentModule = TrigPolly
```

# TrigPolly

Documentation for [TrigPolly](https://github.com/mcamp/TrigPolly.jl).

```@index
```

```@autodocs
Modules = [TrigPolly]
```
