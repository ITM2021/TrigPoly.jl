module TrigPoly

function TP(y::Array,t::Integer,tm::Integer,tot::Integer)
    n = length(y)
    if tm > n
        t = n
    else
        if tm < 1
            t = 1
        end
        t = t
    end
    if t > n
        t = n
    end
    m = (last(y) - first(y)) / (n-1)
    b = first(y)
    slope_line = [(m*x)+b for x in collect(0:n-1)]
    desloped_y = y - slope_line

    afac = 2/(n-1)
    s = n/tot

    TPa = zeros(n,t);
    for i=0:n-1, j=0:t-1
        TPa[i+1,j+1]=sin((j*π)*(i/(n-1)))
    end

    a = zeros(n)
    for j=1:t
        tmp=0
        for i=1:n
            tmp += desloped_y[i] * TPa[i,j]  
        end
        a[j] = tmp * afac
    end

    TPmodel = zeros(tot);
    for x in 0:tot-1
        tp = 0
        for j in 0:t-1
            tp = tp + (a[j+1] * sin( j * π * ((x * s)/(n-1))));
        end
        TPmodel[x+1] = (m * (x * s) + b) + tp
    end

    X1 = collect(0:tot-1) * s
    Y1 = TPmodel
    return X1,Y1,a
end

export TP

end
