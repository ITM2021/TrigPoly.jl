# TrigPolly

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mcamp.gitlab.io/TrigPolly.jl/dev)
[![Build Status](https://github.com/mcamp/TrigPolly.jl/badges/master/pipeline.svg)](https://github.com/mcamp/TrigPolly.jl/pipelines)
[![Coverage](https://github.com/mcamp/TrigPolly.jl/badges/master/coverage.svg)](https://github.com/mcamp/TrigPolly.jl/commits/master)
[![Coverage](https://codecov.io/gh/mcamp/TrigPolly.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/mcamp/TrigPolly.jl)
